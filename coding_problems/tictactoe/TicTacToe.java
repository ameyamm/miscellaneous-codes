public class TicTacToe {
    private static final char [][] winningCombinations = {
            {0,1,2},
            {3,4,5},
            {6,7,8},
            {0,3,6},
            {1,4,7},
            {2,5,8},
            {0,4,8},
            {2,4,6}
        };

    private char[] board;

    public TicTacToe(char[] board) {
        this.board = board;
    }

    public char getWinner() {
        for (int i = 0; i < winningCombinations.length; i++) {
            if ((this.board[winningCombinations[i][0]] != ' ') && 
                (this.board[winningCombinations[i][0]] == this.board[winningCombinations[i][1]]) &&
                (this.board[winningCombinations[i][0]] == this.board[winningCombinations[i][2]])) {
                return this.board[winningCombinations[i][0]];
            }
        }
        return ' ';
    }
}
