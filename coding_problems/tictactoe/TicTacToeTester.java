import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TicTacToeTester {
    @Test
    public void diagonalXWinner() {
        char [] board = {
            'X', 'O', 'O',
            ' ', 'X', 'O',
            'X', 'O', 'X'
        };
        TicTacToe ticTacToe = new TicTacToe(board);
        assertEquals('X', ticTacToe.getWinner());
    }

    @Test
    public void noWinner() {
        char [] board = {
            'X', 'O', 'X',
            'O', 'X', 'O',
            'O', 'X', 'O'
        };
        TicTacToe ticTacToe = new TicTacToe(board);
        assertEquals(' ', ticTacToe.getWinner());
    }

    @Test
    public void topOWinner() {

        char [] board = {
            'O', 'O', 'O',
            'X', 'X', 'O',
            'O', 'X', 'X'
        };

        TicTacToe ticTacToe = new TicTacToe(board);
        assertEquals('O', ticTacToe.getWinner());
    }
}

