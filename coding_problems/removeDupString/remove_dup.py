def remove_dup(s):
    l = list(s)
    l.sort()

    remove_dup_sorted(l)
    print (''.join(l))

def remove_dup_sorted(l):
    back = 0
    fwd = 1

    max_ind = len(l) 
    while fwd < max_ind:
        while fwd < max_ind and l[fwd] == l[back] :
            fwd += 1

        # diff char found
        back += 1
        if fwd == max_ind: break

        #print("Before: {}:{}:{}".format(back, fwd, l))
        # swap
        tmp = l[back]
        l[back] = l[fwd]
        l[fwd] = tmp
        #print("After: {}:{}:{}".format(back, fwd, l))
        fwd += 1


    del(l[back:])
    
if __name__ == '__main__':
    remove_dup('aabaabbaaa')
    remove_dup('aaaaaaa')
    remove_dup('acbacbbaaa')
