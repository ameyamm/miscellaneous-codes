
def get_n_fib(n):

    try: 
        if n < len(get_n_fib.fib): 
            return get_n_fib.fib[n-1]

        get_n_fib.fib.append(0)
        get_n_fib.fib.append(1)

        for i in range(2,n):
            get_n_fib.fib.append(get_n_fib.fib[i-1] + get_n_fib.fib[i-2])

        return get_n_fib.fib[n-1]
    except Exception as e:
        get_n_fib.fib = []
        return get_n_fib(n)

print("{}: {}".format(8,get_n_fib(8)))
print("{}: {}".format(3,get_n_fib(3)))
print("{}: {}".format(5,get_n_fib(5)))
print(get_n_fib.fib)
