import unittest
from tictactoe import getWinner

class TestTicTacToe(unittest.TestCase):

    def setUp(self):
        pass

    def testXDiagonalWinner(self):
        board = [
                    'x','o','x',
                    'o','x','o',
                    'o',None,'x'
                ]
        self.assertEqual('x',getWinner(board));

    def testNoWinner(self):
        board = [
                    'x','o','x',
                    'o','x','o',
                    'o',None,None
                ]
        self.assertEqual(None,getWinner(board));

    def testRaiseException(self):
        board = 1
        with self.assertRaises(AssertionError):
            getWinner(board);

if __name__ == '__main__':
    unittest.main()
