winningCombination = [
        (0,1,2),
        (3,4,5),
        (6,7,8),
        (0,3,6),
        (1,4,7),
        (2,5,8),
        (0,4,8),
        (2,4,6)
        ]

def getWinner(board):

    assert (type(board) is list)  

    for combi in winningCombination:

        if (board[combi[0]] and 
               board[combi[0]] == board[combi[1]] and 
               board[combi[0]] == board[combi[2]]
            ):
            return board[combi[0]]

    return None
