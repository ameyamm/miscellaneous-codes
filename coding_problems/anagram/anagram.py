
def is_anagram(s1, s2):

    freq = {}

    num_uniq_char_s1 = 0

    print("{}, {}".format(s1,s2))

    for s in s1:
        if s in freq:
            freq[s] = freq[s] + 1
        else:
            freq[s] = 1
            num_uniq_char_s1 = num_uniq_char_s1 + 1

    print("1: {}".format(freq))
    num_uniq_char_s2 = 0

    for i in range(len(s2)):
        if s2[i] not in freq:
            return False

        freq[s2[i]] = freq[s2[i]] - 1

        if freq[s2[i]] == 0:

            del freq[s2[i]]

            num_uniq_char_s2 = num_uniq_char_s2 + 1

            if (num_uniq_char_s1 == num_uniq_char_s2) : 
                print("2: {}".format(freq))
                return i == len(s2) - 1
    print("3: {}".format(freq))
                        
    return False 

        
