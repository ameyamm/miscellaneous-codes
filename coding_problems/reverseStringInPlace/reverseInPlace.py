def reverse_str_in_place(s):
    slist = list(s)
    print(slist)

    i = 0
    j = len(slist) - 1

    while (i < j):
        temp = slist[i] 
        slist[i] = slist[j]
        slist[j] = temp
        i = i + 1
        j = j - 1 
        print(slist)

    return ''.join(slist)

print(reverse_str_in_place('abcdef'))
print(reverse_str_in_place('abcde'))
