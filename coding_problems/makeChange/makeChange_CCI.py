def makeChange(n, denom) :
    next_denom = 0

    if denom == 25:
        next_denom = 10
    elif denom == 10:
        next_denom = 5
    elif denom == 5:
        next_denom = 1
    elif denom == 1:
        return 1

    ways = 0
    i = 0
    while i * denom <= n:
        ways += makeChange(n - i * denom, next_denom)
        i = i + 1

    return ways

print(makeChange(10,25))
