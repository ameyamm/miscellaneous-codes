

def makeChange(n):

    if n < 0 : return 0

    if n == 0 : return 1

    ways = 0

    print("ways:{}; n:{}".format(ways, n))
    if n - 25 >= 0:
        print("Calling makeChange(n-25); ways:{}; n:{}".format(ways, n))
        ways = ways + makeChange(n-25)
        print("Returning makeChange(n-25); ways:{}; n:{}".format(ways, n))
    if n - 10 >= 0:
        print("Calling makeChange(n-10); ways:{}; n:{}".format(ways, n))
        ways = ways + makeChange(n-10)
        print("Returning makeChange(n-10); ways:{}; n:{}".format(ways, n))
    if n - 5 >= 0:
        print("Calling makeChange(n-5); ways:{}; n:{}".format(ways, n))
        ways = ways + makeChange(n-5)
        print("Returning makeChange(n-5); ways:{}; n:{}".format(ways, n))
    if n - 1 >= 0:
        print("Calling makeChange(n-1); ways:{}; n:{}".format(ways, n))
        ways = ways + makeChange(n-1)
        print("Returning makeChange(n-1); ways:{}; n:{}".format(ways, n))
    return ways

print(makeChange(10))
