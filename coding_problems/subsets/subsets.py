def get_all_subsets(mySet):

    subsets = list()

    setList = list(mySet)

    # get max combinations
    maxSubsets = 1 << len(setList) 

    for i in range(0, maxSubsets):
        newSubset = set()
        bit = 0 # indicator of the element index in the set to be selected
        # each bit in k indicates whether the corresponding element in the set
        # is selected or not
        k = i # current number in the range of 0 to maxSubsets-1
        while k > 0:  # look for number of 1s in the binary number 
            if k & 1: # if the LSB is 1
                newSubset.add(setList[bit]) # set element indicated by the bit is added to the subset
            k = k >> 1 
            bit = bit + 1
        subsets.append(newSubset)

    return subsets

if __name__ == "__main__":
    print(get_all_subsets(set(['a','b','c'])))
                

