class Node:
    def __init__(self, key, data):
        self.key = key
        self.data = data
        self.prev = None
        self.next = None

class LRUCache: 

    cache_size = 5

    def __init__(self):
        self.cacheMap = {}
        self.head = None
        self.tail = None
        pass

    def get(self, key):
        if key in cacheMap:
            node = self.cacheMap[key]
            self.__remove(node)
            self.__insert_at_head(node)
            return node.data
        else: 
            return -1

    def set(self, key, value):
        # update the value
        if key in self.cacheMap:
            node = self.cacheMap[key]
            node.data = value
            self.__remove(node)
            self.__insert_at_head(node)
        else:
            n = Node(key,value)
            if len(self.cacheMap) <= LRUCache.cache_size:
                self.cacheMap[key] = n
                self.__insert_at_head(n)
            else: 
                del(cacheMap[self.tail.key])
                self.__remove(tail)
                self.__insert_at_head(n)
                self.cacheMap[key] = n

    def __remove(self, n):
        if n.prev != None:
            n.prev.next = n.next
        else:
            self.head = n.next

        if n.next != None:
            n.next.prev = n.prev
        else: 
            self.end = n.prev

    def __insert_at_head(self, n):
        n.next = self.head
        n.prev = None
        if self.head:
            self.head.prev = n
        self.head = n
        if self.end == None:
            self.end = self.head

        pass

    def __remove_the_tail(self):
        pass


